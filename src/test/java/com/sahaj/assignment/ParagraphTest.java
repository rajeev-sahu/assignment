package com.sahaj.assignment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParagraphTest
{
    private Paragraph paragraph;

    @Before
    public void setup()
    {
        paragraph = new Paragraph("Creating random paragraph. It's first line. It's last line of paragraph.");
    }

    @Test
    public void shouldReturnNonEmptyArray()
    {
        final String[] sentences = paragraph.getSentences();

        assertEquals(sentences.length, 3);
    }

}
