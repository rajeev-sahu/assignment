package com.sahaj.assignment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AnswerExtractorTest
{
    private AnswerExtractor answerExtractor;

    @Before
    public void setup()
    {
        answerExtractor = new AnswerExtractor("random string to:1;random string to:2;random string to:3;");
    }

    @Test
    public void shouldReturnNonEmptyArray()
    {
        final Answer[] answers = answerExtractor.getAnswers();

        assertEquals(answers.length, 3);
    }
}
