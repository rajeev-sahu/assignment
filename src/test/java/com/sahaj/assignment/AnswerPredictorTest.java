package com.sahaj.assignment;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class AnswerPredictorTest
{

    private Paragraph paragraph;
    private Question question;
    private Answer[] providedAnswers;

    @Before
    public void setup()
    {
        paragraph = new Paragraph("This is my assignment for Sahaj. Sahaj is located in Bangalore.");
        question = new Question("Where is Sahaj located?");
        providedAnswers = new Answer[]{new Answer("bangalore")};
    }

    @Test
    public void shouldGiveCorrectAnswer()
    {
        final String predict = AnswerPredictor.predict(paragraph, question, providedAnswers).get().trim();

        assertThat(predict, is("bangalore"));
    }

    @Test
    public void shouldGiveNoAnswer()
    {
        final String predict = AnswerPredictor.predict(paragraph, new Question("What is your name?"), providedAnswers).get();

        assertThat(predict, is("<Could not predict answer>"));
    }
}
