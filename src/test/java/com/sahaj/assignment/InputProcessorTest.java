package com.sahaj.assignment;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


public class InputProcessorTest
{
    private String file;

    @Before
    public void setup()
    {
        file = "input.txt";
    }

    @Test
    public void shouldHaveParagraph()
    {
        final InputProcessor inputProcessor = new InputProcessor(file);
        final String paragraph = inputProcessor.getParagraph();

        assertNotNull(paragraph);
    }

    @Test
    public void shouldHaveAListOfQuestions()
    {
        final InputProcessor inputProcessor = new InputProcessor(file);
        final List<Question> questions = inputProcessor.getQuestions();

        assertNotNull(questions);
        assertThat(questions, hasSize(5));
    }


    @Test
    public void shouldHaveAnswer()
    {
        final InputProcessor inputProcessor = new InputProcessor(file);
        final String answers = inputProcessor.getAnswers();

        assertNotNull(answers);
    }

}
