package com.sahaj.assignment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QuestionTest
{
    private Question question;

    @Before
    public void setup()
    {
        question = new Question("What is your name?");
    }

    @Test
    public void shouldReturnNonEmptyArray()
    {
        final String[] questionKeyWords = question.getKeyWords();

        assertEquals(questionKeyWords.length, 4);
    }
}
