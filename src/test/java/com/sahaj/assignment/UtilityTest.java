package com.sahaj.assignment;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class UtilityTest
{
    private Map<String, Integer> map;
    private String[] upperCaseStringArray;

    @Before
    public void setup()
    {
        map = IntStream.rangeClosed(1, 10).boxed()
                       .collect(Collectors.toMap(String::valueOf, e -> e * e));

        upperCaseStringArray = new String[]{"UpperCaseString", "UpperCase"};
    }

    @Test
    public void shouldLowerAllStringArrayElements()
    {
        final String[] result = Utility.toLowerCase(upperCaseStringArray);
        assertEquals(result.length, 2);

        Assert.assertArrayEquals(new String[]{"uppercasestring", "uppercase"}, result);
    }

    @Test(expected = NullPointerException.class)
    public void toLowerCaseShouldThrowExceptionIfCalledWithNullMap()
    {
        Utility.toLowerCase(null);
    }

    @Test
    public void shouldReturnKeyWhichHasMaximumValue()
    {
        final String keyWhichHasMaximumValue = Utility.getKeyWhichHasMaximumValue(map);

        assertThat(keyWhichHasMaximumValue, is("10"));
    }

    @Test(expected = NullPointerException.class)
    public void getKeyWhichHasMaximumValueShouldThrowExceptionIfCalledWithNullMap()
    {
        Utility.getKeyWhichHasMaximumValue(null);
    }


    @Test
    public void shouldSumOfNumbers()
    {
        final Integer sumOfValues = Utility.getSumOfValues(map);

        assertThat(sumOfValues, is(385));
    }

    @Test(expected = NullPointerException.class)
    public void getSumOfValuesShouldThrowExceptionIfCalledWithNullMap()
    {
        Utility.getSumOfValues(null);
    }

    @Test
    public void shouldRemoveWords()
    {
        final String[] result = Utility.remove(StringUtils.split("writing tests is a good habit", " "),
                                               new String[]{"is", "a"});

        Assert.assertArrayEquals(new String[]{"writing", "tests", "good", "habit"}, result);
    }

    @Test(expected = NullPointerException.class)
    public void removeShouldThrowExceptionIfCalledWithNullSource()
    {
        Utility.remove(null, new String[]{"is", "a"});
    }
}
