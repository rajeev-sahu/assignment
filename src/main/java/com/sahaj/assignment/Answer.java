package com.sahaj.assignment;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Objects;

class Answer
{
    private final String answer;

    Answer(final String question)
    {
        this.answer = Objects.requireNonNull(question, "Invalid Answer Format");
    }

    String get()
    {
        return answer.trim();
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                .append(answer)
                .toString();
    }
}
