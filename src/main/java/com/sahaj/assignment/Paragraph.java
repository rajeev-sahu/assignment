package com.sahaj.assignment;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

class Paragraph
{
    private final String paragraph;
    private static final String SEPARATOR = ".";

    Paragraph(final String inputSentence)
    {
        paragraph = Objects.requireNonNull(inputSentence, "Empty paragraph");
    }

    String[] getSentences()
    {
        return StringUtils.split(paragraph, SEPARATOR);
    }
}
