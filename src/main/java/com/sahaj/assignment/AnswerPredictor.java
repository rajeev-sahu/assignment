package com.sahaj.assignment;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.sahaj.assignment.Utility.getKeyWhichHasMaximumValue;
import static com.sahaj.assignment.Utility.getSumOfValues;

class AnswerPredictor
{
    private final static String[] STOP_WORDS = new String[]{"where","what", "of", "is", "the", "which", "are", "and", "to", "there", "that", "a"};

    private static final Answer COULD_NOT_PREDICT_ANSWER = new Answer("<Could not predict answer>");

    static Answer predict(final Paragraph paragraph, final Question question, final Answer[] providedAnswers)
    {
        final Optional<String> matchingSentence = getMatchingSentence(Utility.toLowerCase(paragraph.getSentences()),
                                                                      Utility.toLowerCase(question.getKeyWords()));

        if (matchingSentence.isPresent())
        {
            for (Answer answer : providedAnswers)
            {
                final String matchingSentenceFromParagraph = matchingSentence.get().trim();

                final String matchingSentenceFromProvidedAnswers = answer.get().trim();

                if (matchingSentenceFromParagraph.contains(matchingSentenceFromProvidedAnswers))
                {
                    return answer;
                }
            }
        }

        return COULD_NOT_PREDICT_ANSWER;
    }

    private static Optional<String> getMatchingSentence(final String[] sentences,
                                                        final String[] questionKeywords)
    {

        final String[] questionKeywordsWithoutStopWords = getQuestionKeywordsWithoutStopWords(questionKeywords);

        final Map<String, Integer> allQuestionKeywordsMatchedMap = new HashMap<>();

        int matchingKeywordsCount = -1;
        String sentenceWhichHasMaximumMatchingQuestionKeywords = "";

        for (String sentence : sentences)
        {
            final String[] sentenceKeywords = StringUtils.split(sentence, " ");

            final HashMap<String, Integer> questionKeywordToCountMap = new HashMap<>();

            for (String questionKeyword : questionKeywordsWithoutStopWords)
            {
                for (String sentenceKeyWord : sentenceKeywords)
                {
                    if (!ArrayUtils.contains(STOP_WORDS, sentenceKeyWord))
                    {
                        Integer questionKeywordCount = questionKeywordToCountMap.getOrDefault(questionKeyword,
                                                                                              0);

                        if (sentenceKeywordMatchesQuestionKeyword(questionKeyword,
                                                                  sentenceKeyWord))
                        {
                            questionKeywordCount++;
                        }
                        questionKeywordToCountMap.put(questionKeyword, questionKeywordCount);
                    }
                }
            }

            if (!questionKeywordToCountMap.isEmpty())
            {
                final Integer questionKeywordMatchCount = getSumOfValues(questionKeywordToCountMap);

                if (questionKeywordToCountMap.values().stream().noneMatch(e -> e == 0))
                {
                    allQuestionKeywordsMatchedMap.put(sentence, questionKeywordMatchCount);
                }
                else if (questionKeywordMatchCount > matchingKeywordsCount)
                {
                    sentenceWhichHasMaximumMatchingQuestionKeywords = sentence;
                    matchingKeywordsCount = questionKeywordMatchCount;
                }
            }
        }

        return getMostProbableMatchingSentence(allQuestionKeywordsMatchedMap, sentenceWhichHasMaximumMatchingQuestionKeywords);
    }

    private static Optional<String> getMostProbableMatchingSentence(final Map<String, Integer> allQuestionKeywordsMatchedMap,
                                                                    final String sentenceWhichHasMaximumMatchingQuestionKeywords)
    {
        if (!allQuestionKeywordsMatchedMap.isEmpty())
        {
            return allQuestionKeywordsMatchedMap.size() == 1 ? allQuestionKeywordsMatchedMap.keySet()
                                                                                            .stream()
                                                                                            .findFirst()
                                                             : Optional.of(getKeyWhichHasMaximumValue(allQuestionKeywordsMatchedMap));
        }
        return Optional.of(sentenceWhichHasMaximumMatchingQuestionKeywords);
    }

    private static boolean sentenceKeywordMatchesQuestionKeyword(final String questionKeyword,
                                                                 final String sentenceKeyWord)
    {
        return sentenceKeyWord.equals(questionKeyword) ||
                sentenceKeyWord.contains(questionKeyword) ||
                questionKeyword.contains(sentenceKeyWord);
    }

    private static String[] getQuestionKeywordsWithoutStopWords(final String[] questionKeywords)
    {
        return Utility.remove(questionKeywords, STOP_WORDS);
    }
}
