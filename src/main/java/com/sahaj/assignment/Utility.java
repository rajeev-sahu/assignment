package com.sahaj.assignment;

import org.apache.commons.lang3.ArrayUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

class Utility
{
    static String[] toLowerCase(String[] inputArray)
    {
        return Stream.of(inputArray).map(String::toLowerCase).toArray(String[]::new);
    }

    static String getKeyWhichHasMaximumValue(final Map<String, Integer> inputMap)
    {
        Objects.requireNonNull(inputMap, "Invalid inputMap");

        String sentenceWhichHasMaximumMatchingQuestionKeywords = "";
        int maximumKeywordsCount = -1;

        for (String sentence : inputMap.keySet())
        {
            if (inputMap.get(sentence) > maximumKeywordsCount)
            {
                maximumKeywordsCount = inputMap.get(sentence);
                sentenceWhichHasMaximumMatchingQuestionKeywords = sentence;
            }
        }
        return sentenceWhichHasMaximumMatchingQuestionKeywords;
    }

    static int getSumOfValues(final Map<String, Integer> inputMap)
    {
        Objects.requireNonNull(inputMap, "Invalid inputMap");

        return inputMap.values()
                       .stream()
                       .mapToInt(Integer::intValue)
                       .sum();
    }

    static String[] remove(final String[] source, final String[] wordsToRemove)
    {
        Objects.requireNonNull(source, "Invalid source");

        return Stream.of(source)
                     .filter(questionKeyword -> !ArrayUtils.contains(wordsToRemove, questionKeyword))
                     .toArray(String[]::new);
    }
}
