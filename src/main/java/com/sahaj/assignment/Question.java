package com.sahaj.assignment;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Objects;

class Question
{
    private final String question;

    Question(final String question)
    {
        this.question = Objects.requireNonNull(question, "Invalid Question Format");
    }

    private String get()
    {
        return StringUtils.remove(question, "?");
    }

    String[] getKeyWords()
    {
        return StringUtils.split(get(), " ");
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                .append(question)
                .toString();
    }
}
