package com.sahaj.assignment;

import java.util.Map;
import java.util.stream.Collectors;

public class ApplicationRunner
{
    public static void main(String[] args)
    {
        final InputProcessor inputProcessor = new InputProcessor("input.txt");

        final Paragraph paragraph = new Paragraph(inputProcessor.getParagraph());

        final Answer[] providedAnswers = new AnswerExtractor(inputProcessor.getAnswers()).getAnswers();

        final Map<Question, Answer> questionAnswerMap = inputProcessor.getQuestions()
                                                                      .stream()
                                                                      .collect(Collectors.toMap(question -> question,
                                                                                                question -> AnswerPredictor
                                                                                                        .predict(paragraph,
                                                                                                                 question,
                                                                                                                 providedAnswers)));

        questionAnswerMap.forEach((question, answer) -> System.out.println(question + "\n" + answer + "\n"));
    }
}
