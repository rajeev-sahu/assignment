package com.sahaj.assignment;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.stream.Stream;

class AnswerExtractor
{
    private static final String SEPARATOR = ";";
    private final String answerString;

    AnswerExtractor(final String answerString)
    {
        this.answerString = Objects.requireNonNull(answerString, "Empty answers");
    }

    private String[] extract()
    {
        return StringUtils.split(answerString, SEPARATOR);
    }

    Answer[] getAnswers()
    {
        return Stream.of(Utility.toLowerCase(extract()))
                     .map(Answer::new)
                     .toArray(Answer[]::new);
    }
}
