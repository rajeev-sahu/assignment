package com.sahaj.assignment;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.spi.FileSystemProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class InputProcessor
{
    private final String paragraph;
    private final List<Question> questions;
    private final String answers;

    InputProcessor(final String filePath)
    {
        try
        {
            final List<String> lines = Files.lines(getFilePath(filePath)).collect(Collectors.toList());

            paragraph = lines.get(0);

            questions = lines.subList(1, 6)
                             .stream()
                             .map(Question::new)
                             .collect(Collectors.toList());

            answers = lines.get(6);
        }
        catch (URISyntaxException | IOException e)
        {
            throw new RuntimeException("Couldn't process input file");
        }
    }

    private Path getFilePath(final String filePath) throws URISyntaxException, IOException
    {
        final URI uri = Objects.requireNonNull(getClass().getClassLoader().getResource(filePath)).toURI();

        if ("jar".equals(uri.getScheme()))
        {
            for (FileSystemProvider provider : FileSystemProvider.installedProviders())
            {
                if ("jar".equalsIgnoreCase(provider.getScheme()))
                {
                    try
                    {
                        provider.getFileSystem(uri);
                    }
                    catch (FileSystemNotFoundException e)
                    {
                        provider.newFileSystem(uri, Collections.emptyMap());
                    }
                }
            }
        }
        return Paths.get(uri);
    }

    String getParagraph()
    {
        return paragraph;
    }

    List<Question> getQuestions()
    {
        return new ArrayList<>(questions);
    }

    String getAnswers()
    {
        return answers;
    }
}
